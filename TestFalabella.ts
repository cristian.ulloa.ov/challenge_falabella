//Arreglo de string: 'Falabella' multiplos de 3, 'IT' multiplos de 5, 'Integraciones' múltiplos de 15 (de ambos).
let arreglo_string: Array<string> = ["Falabella", "IT", "Integraciones"];

for (let i=1; i <= 100; i++){
    let imprimir: string = '';
    if (i%3 == 0 || i%5 == 0){
        //Se obtiene de manera dinámica el valor de la posición del arreglo, según sea el caso de múltilplos de 3, 5 o ambos.
        imprimir = arreglo_string[Number(Number(i % 5 == 0 && i % 3 >= 1) + Number(i % 3 == 0 && i % 5 == 0)*2)];
    }
    console.log('Número ' + i + ' ' +  imprimir);
}
