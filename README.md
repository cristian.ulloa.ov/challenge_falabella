# Challenge_Falabella
***
Solución de Test Técnico planteado por Falabella.

## Problema
Write a program that prints all the numbers from 1 to 100. However, for
multiples of 3, instead of the number, print "Falabella". For multiples of 5 print
"IT". For numbers which are multiples of both 3 and 5, print "Integraciones".
But here's the catch: you can use only one `if`. No multiple branches, ternary
operators or `else`.

### Solución
Desarrollado en Lenguaje TypeScript.
Se crea un arreglo de String con los valores presentados por imprimir. Se obtiene de manera dinámica el valor de la posición del arreglo, según sea el caso de múltilplos de 3, 5 o ambos.

